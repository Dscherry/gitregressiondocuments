
<center> <h2>Draft</h2> </center>

**Action Plan: Regression Apprenticeship Program**
***

## Overview

The Regression Apprenticeship Program is designed to be delivered in 3 phases:

* Spring 2021 Workshop (all Support)
* Apprenticeship Program (4-5 candidates) ([^1]).
* Support Workday Training (all Support)

***

## Workshop

Topics for Spring Workshop 2021: Testing Outside the Core ([^2])

* Create a Volunteer in CDH
* Create a Volunteer using an Existing Volunteer in CDH
* Suspend Volunteers: Suspend Other, Suspend Role Concluded
* ~~Simplified Test Tour Strategy (introduce consolidation)~~
* Risk Assessment
* Exploratory Testing: How to Design a Test Charter ([^3])
 / Tour Tool
* GitLab Fundamentals (view) - /not workshop 
  
***

## Apprenticeship

The Regression Apprenticeship training outline: 

* Regression Strategies ([^4])
* Feature File Characteristics: Review
* Bug Writing Process
* Critical Thinking Skills
    * Observation
    * Analysis
    * Inference
    * Communication
    * Problems Solving

***
#### Footnotes
* 1
 Ideal candidates have time to complete assigned work in preparation for coaching session and can attended at least
two 90-minute coaching sessions per week.
* 2
 Some of the topics can be delivered at the Support Workday if adequate time for training is not available at the
workshop.
* 3
 Deliverable includes tracking system.
* 4
 Regression Strategy.pptx, 12/2/2020 